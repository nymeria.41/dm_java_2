import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;


public class BibDM {

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a+b;
    }
    

    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
        if (liste.size()==0){
            return null;
        }
        Integer min=liste.get(0);
        for (Integer elem : liste){
            if ((elem-min)<0){
                min=elem;
            }
        }
        return min;
    }
    
    
    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur 
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        if (liste.size()==0){
            return true;
        }
        else{
            for ( T elem : liste){
                if (elem.compareTo(valeur)<=0){
                    return false;
                }
            }
            return true;
    }
}


    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
    */

    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
        List<T> res = new ArrayList<T>();
        int i = 0;
        int j=0;
        while(i<liste1.size() && j<liste2.size()){
            if (liste1.get(i).compareTo(liste2.get(j))==0){
                if (res.contains(liste1.get(i))){
                    i++;
                    j++;
                }
                else{
                    res.add(liste1.get(i));
                    i++;
                    j++;
                    
                }
            }
            else if (liste1.get(i).compareTo(liste2.get(j))>0){
                j++;
            }
            else{
                i++;
            } 
        }
        return res;
    }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
        List<String> res = new ArrayList<String>();
        if(texte.length() == 0){
            return res;
        }

        String mot = "";
        for(int i=0; i<texte.length(); i++){
            if (texte.charAt(i)==' ' ){
                if(!(mot == "")){
                    res.add(mot);
                }
                mot="";
            }
            else{
                mot+=texte.charAt(i);
            }
        }
        if(!(mot == "")){
            res.add(mot);
        }

    return res;
}
        

    

    /** 
     * transforme une liste en dictionnaire avec comme clef les éléments de la liste et comme
     * valeur leur nombre d'apparition dans la liste
     * */
    public static HashMap <String,Integer> faitDicoApparition( List<String> liste){
        HashMap<String,Integer> res = new HashMap <String,Integer> ();
        for (String elem : liste ){
            if(!res.keySet().contains(elem)){
                res.put(elem,1);
            }
            else{
                int val=0;
                val=res.get(elem);
                res.remove(elem);
                res.put(elem,val+1);
            }
        }
        return res;
    }

    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
        if(texte.equals("")){
            return null;
        }
        List<String> liste = decoupe(texte);
        HashMap<String,Integer> dico = faitDicoApparition(liste);
        String res=liste.get(0);        
        int valMax = dico.get(res) ;
        int val=0;
        
        for (String mot : dico.keySet() ){
            val=dico.get(mot);
            if (val>valMax){
                valMax =val;
                res=mot;
            }
            else{
                if(val == valMax){
                    if(mot.compareTo(res)<0)
                        res=mot;
                }

            }
        }
        return res;
    }
   
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parenthèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static Boolean bienParenthesee(String chaine){
        
        if(chaine.equals("")){return true;}
       
        if ( chaine.startsWith(")") || chaine.endsWith("(")){
            return false;
        }

        int cpt= 0;

        for(int i=0;i<chaine.length();i++){
            char carac = chaine.charAt(i);
            if (carac=='('){
                cpt+=1;
            }
            if (carac==')'){
                cpt-=1;
            }
        }
        return cpt==0;
        
    }    

    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static Boolean bienParentheseeCrochets(String chaine){

        if(chaine.equals("")){return true;}
       
        if ( chaine.startsWith("]") || chaine.endsWith("[")){
            return false;
        }
        int i= 0;
        int j=0;

        for ( char c : chaine.toCharArray()){
            if (c=='['){
                i+=1;
            }
            else if (c==']'){
                j+=1;
            }
        }
        return i==j && bienParenthesee(chaine) ;
    }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
        int deb=0;
        int fin=liste.size();
        int mil;

        while((deb<fin)){
            mil = (deb + fin)/2;

            if(liste.get(mil)<valeur){
                deb=mil+1;
            }
            else{fin=mil;}        
        }    
    return deb<liste.size() && valeur == liste.get(deb);
    }

}
